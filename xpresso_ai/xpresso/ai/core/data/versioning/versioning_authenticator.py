__all__ = ['VersioningAuthenticator']
__author__ = 'Gopi Krishna'

from os import path
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.commons.network.http.send_request import SendHTTPRequest
from xpresso.ai.core.commons.network.http.http_request import HTTPMethod
from xpresso.ai.core.commons.exceptions.xpr_exceptions \
    import AuthenticationFailedException, InvalidEnvironmentException, \
    RepoPermissionException
from xpresso.ai.core.commons.utils.constants import config_paths,\
    USER_CONTEXT, CONTEXT_PROJECT_LIST_KEY, CONTEXT_USER_INFO_KEY, \
    PRIMARY_ROLE, SUPER_USER, DV_PROJECT_TOKEN, DV_USER_TOKEN_FLAG, \
    DV_PROJECT_TOKEN_FLAG, CONTROLLER_FIELD, CLIENT_PATH_FIELD, \
    SERVER_URL_FIELD, PROJECT_TOKEN_IN_PROJECTS, DV_INPUT_REPO_NAME, \
    PROJECT_NAME_IN_PROJECTS


class VersioningAuthenticator:
    """
    Authenticates the requests for using Data versioning module
    """
    def __init__(self, **kwargs):
        """
        Class constructor
        """
        self.config_path = XprConfigParser.DEFAULT_CONFIG_PATH
        default_config = XprConfigParser(self.config_path)
        self.base_path = path.join(
            path.expanduser('~'),
            default_config[CONTROLLER_FIELD][CLIENT_PATH_FIELD])
        self.kwargs = kwargs if kwargs else {}
        self.token_flag = DV_PROJECT_TOKEN_FLAG if DV_PROJECT_TOKEN in kwargs \
            else DV_USER_TOKEN_FLAG
        self.config = default_config if DV_PROJECT_TOKEN in kwargs else None

    def update_config(self):
        """
        fetches the config values as per user entrance point to
         PachydermVersionController and saves it into config attribute
        """
        if DV_PROJECT_TOKEN in self.kwargs:
            # If project_token is present, default config is considered
            self.token_flag = DV_PROJECT_TOKEN_FLAG
            self.config = XprConfigParser()
            return
        # If project_token not found, then local workspace, token is verified
        workspace_env = self.get_workspace_env()
        path_to_config = config_paths[workspace_env]
        self.config_path = path_to_config
        self.config = XprConfigParser(path_to_config)
        self.base_path = path.join(
            path.expanduser('~'),
            self.config[CONTROLLER_FIELD][CLIENT_PATH_FIELD])

    def get_token(self):
        """Token is saved in the local file system for """
        if self.token_flag == DV_PROJECT_TOKEN_FLAG:
            return self.kwargs[DV_PROJECT_TOKEN]
        try:
            token_file_path = f"{self.base_path}.current"
            with open(token_file_path, "r") as f:
                token = f.read()
                token = token.strip("\n")
                return token
        except FileNotFoundError:
            raise AuthenticationFailedException(
                "No session found. Please login."
            )

    def get_workspace_env(self):
        try:
            workspace_file = f'{self.base_path}.workspace'
            with open(workspace_file, "r") as env_file:
                env = env_file.read()
        except FileNotFoundError:
            raise InvalidEnvironmentException("workspace env file not found."
                                              " Login again.")
        return env

    def check_token(self, token):
        """
        checks if the token is valid or not
        Args:
            token: login token saved on user system
        Returns:
            returns response for token authentication request from server
        """
        server_endpoint = self.config[CONTROLLER_FIELD][SERVER_URL_FIELD]
        if self.token_flag == DV_USER_TOKEN_FLAG:
            url = f"{server_endpoint}/versioning/auth"
            request_header = {"token": token}
            request_body = {}
        else:
            url = f"{server_endpoint}/project/api"
            request_header = {}
            request_body = {
                PROJECT_TOKEN_IN_PROJECTS: token
            }
        token_response = SendHTTPRequest().send(
            url=url, http_method=HTTPMethod.GET,
            header=request_header,
            data=request_body
        )
        return token_response

    def authenticate_session(self):
        """
        authenticates the login session by checking if there is a login
        """
        if self.token_flag == DV_PROJECT_TOKEN_FLAG:
            return
        if not self.config:
            self.update_config()
        token = self.get_token()
        self.check_token(token)

    def authenticate_request(self, caller_method):
        """
        authenticates any new request to data versioning module
        that needs access level and login expiry checks
        """
        def auth_decorator(*args, **kwargs):
            if not self.config:
                self.update_config()
            token = self.get_token()
            token_response = self.check_token(token)
            if DV_INPUT_REPO_NAME in kwargs:
                self.validate_repo_access(
                    kwargs[DV_INPUT_REPO_NAME],
                    token_response
                )
            return caller_method(*args, **kwargs)
        return auth_decorator

    def validate_repo_access(self, repo_name: str, token_response: dict):
        """
        validates if the user has access to the repo or not
        By default, xpresso super user has access to everything

        :param repo_name:
            name of the repo
        :param token_response:
            response received after sending request to server
        """
        if self.token_flag == DV_PROJECT_TOKEN_FLAG:
            project_name = token_response[PROJECT_NAME_IN_PROJECTS]
            if project_name != repo_name:
                raise RepoPermissionException()
            return
        user_info = token_response[USER_CONTEXT][CONTEXT_USER_INFO_KEY]
        if user_info[PRIMARY_ROLE].lower() == SUPER_USER:
            return
        project_list = token_response[USER_CONTEXT][CONTEXT_PROJECT_LIST_KEY]
        if repo_name not in project_list:
            raise RepoPermissionException(
                "Either you do not have access to this repo"
                " or it does not exist."
            )

    def filter_repo(self, caller_method):
        """
        decorator method to filter the output for list_repo
        Args:
            caller_method:
        Returns:
             returns list of repos
        """
        def filter_decorator(*args, **kwargs):
            if not self.config:
                self.update_config()
            token = self.get_token()
            # validates token
            token_response = self.check_token(token)
            repo_list_info = caller_method(*args, **kwargs)
            if self.token_flag == DV_PROJECT_TOKEN_FLAG:
                filter_repo_name = token_response[PROJECT_NAME_IN_PROJECTS]
                filter_repo_info = []
                for repo_item in repo_list_info:
                    if repo_item[DV_INPUT_REPO_NAME] == filter_repo_name:
                        filter_repo_info.append(repo_item)
                        break
                if not len(filter_repo_info):
                    raise RepoPermissionException
                return filter_repo_info
            user_info = token_response[USER_CONTEXT][CONTEXT_USER_INFO_KEY]
            if user_info[PRIMARY_ROLE].lower() == SUPER_USER:
                return repo_list_info
            project_list = \
                token_response[USER_CONTEXT][CONTEXT_PROJECT_LIST_KEY]
            filtered_repo_list = []
            for repo_item in repo_list_info:
                if repo_item[DV_INPUT_REPO_NAME] in project_list:
                    filtered_repo_list.append(repo_item)
            if not len(filtered_repo_list):
                raise RepoPermissionException(
                    "Unable to find any repos you can access"
                )
            return filtered_repo_list
        return filter_decorator
