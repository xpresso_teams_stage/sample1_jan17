""" FSConnector class for connectivity to various filesystems """

__author__ = 'Shlok Chaudhari'
__all__ = 'connector'


import os
import xpresso.ai.core.commons.utils.constants as constants
import xpresso.ai.core.data.connections.connector_exception as connector_exception
from xpresso.ai.core.data.connections.abstract_fs import AbstractFSConnector
from xpresso.ai.core.data.connections.external.alluxio import client as alluxio
from xpresso.ai.core.logging.xpr_log import XprLogger


class FSConnector(AbstractFSConnector):
    """

    DataConnector class for connecting to filesystems

    """

    def __init__(self):
        """

        __init__() here initializes the client object needed for
        interacting with datasource API

        """

        self.client = None

    def getlogger(self):
        """

        Xpresso logger built on top of python module

        :return: xpresso logging module

        """

        return XprLogger()

    def connect(self, config):
        """

        Connect method to establish client-side connection with a filesystem

        :param config: A JSON object, input by the user, that states the file to
                        be imported

        :return: path: absolute path to a target file/directory (String object)

        """

        self.client = alluxio.AlluxioConnector()
        path = config.get(constants.input_path)
        dataset_type = config.get(constants.dataset_type)
        return path, dataset_type

    def import_dataframe(self, config, **kwargs):
        """

        Importing data from a user specified file in a filesystem

        :param config: A JSON object, input by the user, that states the file to
                        be imported

        Usage:

            config = {
                    "path": "/test_connector/small_size_data/city.txt"
            }

        :return: data_frame: A DataFrame object of the data required from a file

        """

        data_frame = None
        try:
            path, dataset_type = self.connect(config)
            data_frame = self.client.import_data(path, dataset_type, **kwargs)
            self.close()
            self.getlogger().info("Data imported from filesystem.")
        except connector_exception.ConnectorImportFailed as exc:
            self.getlogger().exception("\nInput configurations might be wrong. "
                                       "Check input JSON object.\n\n%s" % str(exc))
        return data_frame

    def import_files(self, config):
        """

        Importing file/files from a user specified filesystem

        :param config: A JSON object, input by the user, that states the file/files
                        to be imported

        Usage:

            - Import Single File
                config = {
                        "path": "/test_connector/small_size_data/city.txt"
                }
            - Import Multiple Files
                config = {
                        "path": "/test_connector/small_size_data/"
                }

        :return: data_frame: A DataFrame object that consists of properties of the
        file/files

        """

        data_frame = None
        try:
            path, dataset_type = self.connect(config)
            file_extension = os.path.splitext(path)[1]
            if file_extension == constants.text_extension or \
                    file_extension == constants.csv_extension or \
                    file_extension == constants.excel_extension:
                data_frame = self.client.save_file_raw(config.get(constants.input_path))
                self.getlogger().info("File %s has been imported."
                                      % config.get(constants.input_path))
            else:
                try:
                    if file_extension[0] == constants.DOT:
                        self.getlogger().error("Error: Unsupported Filetype found")
                except IndexError:
                    data_frame = self.client.save_multiple_files_raw(
                        config.get(constants.input_path))
                    self.getlogger().info("Files from %s directory have been imported."
                                          % config.get(constants.input_path))
            self.close()
        except connector_exception.ConnectorImportFailed as exc:
            self.getlogger().exception("\nInput configurations might be wrong. "
                                       "Check input JSON object.\n\n%s" % str(exc))
        return data_frame

    def close(self):
        """

        Method to close connection to filesystem

        :return: None

        """

        self.client.close()
