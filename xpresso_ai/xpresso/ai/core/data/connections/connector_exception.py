__author__ = 'Shlok Chaudhari'


class Error(Exception):
    pass


class ConnectorInputKeyValueMissing(Error):
    pass


class ConnectorImportFailed(Error):
    pass


class ConnectorInvalidInput(Error):
    pass


class ConnectorInvalidObject(Error):
    pass


class ConnectorInvalidArguments(Error):
    pass


class ConnectorInvalidFilename(Error):
    pass
